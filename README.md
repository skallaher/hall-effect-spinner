# Hall Effect Spinner

Practice project for interacting with both motors and sensors in conjunction.

## Exercise 1

Update the `execute()` method of `SpinnerCommand` to perform the following during
the Autonomous period:

1. Move to the location of the hall effect sensor to calibrate.
2. Spin one revolution.
3. Spin to the following orientations in order. You may return back to `0` after
   each position, but this should not be necessary (bonus points!!). These are
   provided in multiple units for convenience.

    | Order | Revolutions | Degrees | Radians           |
    | ---   | ---         | ---     | ---               |
    | 1     | 0.25        | 90      | $`\pi`$/2         |
    | 2     | 0.75        | 270     | 3$`\pi`$/2        |
    | 3     | 0.125       | 45      | $`\pi`$/4         |
    | 4     | 0.5         | 180     | $`\pi`$           |
    | 5     | 0.16667     | 60      | $`\pi`$/3         |
    | 6     | 1           | 360     | 2$`\pi`$          |
    | 7     | 0           | 0       | 0                 |
    | 8     | 0.588889    | 212     | 1.1777777$`\pi`$  |

4. Return to 0. You may recalibrate if desired.
5. Spin 3 full revolutions and stop back at 0.

Both speed and precision are critical here. Don't overdo one, just to sacrifice
the other.

## Exercise 2

While in Teleop, configure the spinner to perform the following:

| Button            | Pressed?              | Held?                 | Operation |
| ---               | ---                   | ---                   | ---       |
| `A`               | :x:                   | :heavy_check_mark:    | Rotate to the same **absolute** position as the left analog stick, where forward is 0. |
| `B`               | :heavy_check_mark:    | :x:                   | Return to 0 |
| `X`               | :x:                   | :heavy_check_mark:    | Rotate to the same **absolute** position as a potentiometer on the bench |
| `Y`               | :heavy_check_mark:    | :x:                   | Rotate one full rotation, returning to the starting orientation |
| `Back`            | :heavy_check_mark:    | :x:                   | Perform a calibration routine |
| `Start`           | :heavy_check_mark:    | :x:                   | Reset speed to 20% |
| `Left Bumper`     | :heavy_check_mark:    | :x:                   | Decrease the set speed of the spinner by 10% |
| `Right Bumper`    | :heavy_check_mark:    | :x:                   | Increase the set speed of the spinner by 10% |
| `Directional Pad` | :x:                   | :heavy_check_mark:    | Rotate to the given direction, where up is 0 and right is clockwise |

All movement should be done at a maximum of the configured speed.

Some of the actions above are easier than others.

You may include a calibration sequence on start, if you desire.
