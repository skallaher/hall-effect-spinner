// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class SpinnerSubsystem extends SubsystemBase {

  private CANSparkMax spinnerController;

  /** Creates a new SpinnerSubsystem. */
  public SpinnerSubsystem() {
    spinnerController = new CANSparkMax(Constants.SPINNER_ADDRESS, MotorType.kBrushless);
  }

  public void setSpeed(double speed) {
    spinnerController.set(speed);
  }

  public double getSpeed() {
    return spinnerController.get();
  }

  public void calibrate() {
    spinnerController.getEncoder().setPosition(0.0);
  }

  public double getPosition() {
    return spinnerController.getEncoder().getPosition();
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }

  @Override
  public void simulationPeriodic() {
    // This method will be called once per scheduler run during simulation
  }
}
