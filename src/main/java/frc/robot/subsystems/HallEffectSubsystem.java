package frc.robot.subsystems;

import edu.wpi.first.wpilibj.AnalogInput;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class HallEffectSubsystem extends SubsystemBase {

    private AnalogInput sensor;

    private double latestData;

    public HallEffectSubsystem() {
      this.sensor = new AnalogInput(Constants.HALL_EFFECT_ADDR);
    }

    public double getLatestSensorReading() {
      return latestData;
    }
    
    @Override
    public void periodic() {
      // This method will be called once per scheduler run
      latestData = sensor.getVoltage();
    }

    @Override
    public void simulationPeriodic() {
      // This method will be called once per scheduler run during simulation
    }
}
